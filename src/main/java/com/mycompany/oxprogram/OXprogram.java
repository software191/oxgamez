/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class OXprogram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.println("Welcome to OX Game");

        //Create default board 2DArray
        char[][] board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }

        boolean start = true;
        boolean endGame = false;

        while (!endGame) {
            printBoard(board);
            //set symbol O or X
            char symbol = ' ';
            if (start) {
                symbol = 'O';
            } else {
                symbol = 'X';
            }

            if (start) {
                System.out.println("Turn O");
            } else {
                System.out.println("Turn X");
            }
            while (true) {
                System.out.println("Please input row, col: ");
                int row = kb.nextInt();
                int col = kb.nextInt();

                board[row][col] = symbol;
                break;
            }
            if (checkWon(board) == 'O') {
                printBoard(board);
                System.out.println(">>>O Win<<<");
                endGame = true;
            } else if (checkWon(board) == 'X') {
                printBoard(board);
                System.out.println(">>>X Win<<<");
                endGame = true;
            } else {
                if (checkDraw(board)) {
                    printBoard(board);
                    System.out.println(">>>Draw!!<<<");
                    endGame = true;
                } else {
                    start = !start;
                }
            }
        }

    }

    public static void printBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println("");
        }
    }

    public static char checkWon(char[][] board) {
        //chekRow
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return board[i][0];
            }
        }
        //checkCol
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                return board[0][j];
            }
        }
        //diagonal left
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return board[0][0];
        }
        //diagonal right
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
            return board[0][2];
        }

        return ' ';
    }

    public static boolean checkDraw(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
